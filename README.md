# Le fichier htaccess kesaco ?

Les fichiers HTACCESS permettent d'établir des règles qui sont interprétées par le serveur (Apache).
Voici les principales utilisations de ces fichiers :

- Protéger l'accès à un ou plusieurs fichiers, répertoires, et type de fichiers
- Restreindre l'accès avec des règles bien définies
- Faire des redirections
- Personnaliser les messages d'erreur
- etc ...

[Lien vers la documentation officielle](https://httpd.apache.org/docs/2.0/howto/htaccess.html)

## Comment créer un fichier .htaccess

Comme vous pouvez le remarquer le fichier `.htaccess` n'a aucun nom de fichier, il a juste une extension.
Le contenu de ce fichier est du texte, c'est pourquoi vous pouvez le créer sous un éditeur de texte tel que le bloc note. 


## Comment protéger l'accès à son site via un .htaccess

Pour restreindre l'accès, le fichier doit être placé dans le répertoire à protéger, si vous voulez interdire l'accès au site, mettez-le à la racine du serveur web.

Voici le code à insérer dans votre fichier ".htaccess" :

```
AuthUserFile chemin_absolu/.htpasswd
AuthName "Accès protégé"
AuthType Basic
Require valid-user
```

Le fichier ".htpasswd" devra être créé de la même manière que vous venez de créer le fichier ".htaccess".

Attention à où vous mettez ce fichier htpasswd, il ne doit pas être accessible depuis le web sinon celà perdrait toute utilité.


Voici un aperçu du contenu d'un ".htpasswd"

user:mot_de_passe_crypté

Pour crypter votre mot de passe vous pouvez utiliser la fonction php "password_hash"

```php
<? php echo password_hash('damien', PASSWORD_DEFAULT) ?>
```

Sur des serveurs en ligne, il est parfois complexe de connaître le chemin absolu d'un fichier.

Vous avez la méthode realpath en PHP qui peut vous aider à le trouver.

PS : Vous aurez remarqué que le dossier n'est plus listé par défaut par apache. 
Il vous faudra rajouter une option dans la configuration d'apache : 
`̀IndexOptions +ShowForbidden`

## Comment faire une redirection

Pour faire une redirection, il suffit d'intégrer le code ci-dessous :

> RedirectPermanent /  http://www.autresite.com

Cette instruction permet de rediriger la totalité du site vers "http://www.autresite.com",
Le " / " permet de définir dans quel répertoire la redirection doit être active, ici on a défini la racine du site.

Pour la redirection d'une page, voir ci- dessous :

> RedirectPermanent /mapage.php http://www.monsite.com/index.php


## Comment faire une redirection des pages d'erreur ?

Avec un .htaccess vous pouvez aussi rediriger les pages d'erreur, par exemple si vous voulez que les erreurs 404, ( page inexistante) soit redirigées vers l'accueil de votre site, il suffit d'ajouter dans votre fichier .htaccess la ligne :

> ErrorDocument 404 http://www.monsite.com/index.php


## Configurer des paramètres

Il est possible de surcharger le paramétrage d'apache par le biais de ce fichier. Il se peut par exemple que vous souhaitiez augmenter la mémoire allouée.

### PHP Memory Limit
> php_value memory_limit 128M


### Insérer le fuseau horaire
> SetEnv TZ Europe/Paris 


### Bloquer des adresse IP

Il est possible de refuser l’accès de sites Internet à des adresses ou domaines IP. Avec le code adéquat, il est même possible d’interdire l’accès à toutes les adresses IP tout en le garantissant à une poignée. Ainsi, l’offre Internet peut être mise à la disposition de seulement quelques employés sur l’intranet d’une entreprise. La directive suivante résume certaines des limitations d’accès possibles :

#### Fichiers pour réguler les zones IP
```
Order deny,allow
Deny from .aol.com
Deny from 192.168
Allow from 192.168.220.10
```


### Rediriger vers le https

Si vous utilisez un certificat SSL pour votre domaine, il est possible de le rediriger via une directive .htaccess sur une requête HTTPS.

### Activer HTTPS
```
RewriteEngine On
RewriteCond %{Server_Port} !=443
RewriteRule ^(.*)$ https://votre-domaine.fr/$1 [R=301,L]
```
